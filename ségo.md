# Lettre à Ségolène Royal

À l'attention de :
Madame La Ministre de l’Écologie, du Développement durable et de l’Énergie

> « Reconnaissons aux jardins partagés, quand ils sont sauvages d'autant plus, une forme d'appropriation ou de (ré)appropriation de la nature. »
> Benoît Hartmann, porte-parole de France Nature Environnement, sur France inter service publique : Ecologie pourquoi tant d'indifférence?
> [http://www.franceinter.fr/emission-service-public-ecologie-pourquoi-tant-d-indifference](http://www.franceinter.fr/emission-service-public-ecologie-pourquoi-tant-d-indifference)

Madame la Ministre,

Nous souhaiterions porter à votre connaissance cette initiative citoyenne dans
le 1er arrondissement de Lyon, initiative en veille du fait d'un refus de
dialogue de la part de la mairie centrale de Lyon :
*Le jardin des Pendarts*

## Rappel des faits :

Le 28 février 2015 nous avons ouvert, sans autorisation, en plein centre
ville, au 35 rue du bon pasteur à Lyon un jardin autogéré sur un terrain
abandonné depuis plus de 25 ans. Notre idée, créer un lieux d'échange et de
partage, un lieu de réflexion pour commencer à penser le monde autrement. Nous
avons fait les choses proprement (analyse de sol, installation d'un portail)
et avec panache (mise en scène et discours d'ouverture). Nous ne nous
revendiquons d'aucun groupe ou parti. Nous voulons simplement participer à la
vie de notre quartier, façonner celui-ci, agir au présent pour penser demain.

Avec 300 participants dès le 1er jour, nous pensions innocemment que notre
action serait reconnue comme légitime. Pourtant la ville de Lyon nous a envoyé
les huissiers deux jours après ce premier événement public. Alors, avec nos
voisins de l’association Ruche de X-Rousse, occupants sans droit ni titre
d'une maison situé au 33 bis de la rue du bon pasteur, nous avons lancé une
pétition, comptant plus de 3500 signatures, remise à M. Gérard Collomb
sénateur maire de Lyon, le 24 avril en vue d'obtenir une convention
d’occupation temporaire pour le terrain et le bâtiment. Une demande de
régularisation d'occupation à été adressée au Maire de Lyon, avec le soutien
de la maire et des élus du 1er arrondissement, de la LCS Lyon Citoyen
solidaire et d'EELV (europe écologie les verts), avec le soutien des
commerçants, associations, et maison de retraites du quartier, mais tout cela
n'a pas suffi.

« Ce mardi (2 juin 2015) matin, dès 8 heures, une trentaine de CRS ont bloqué
la rue Bon Pasteur, dans le premier arrondissement lyonnais. Marteau-piqueur,
pelleteuse sont entrés en action pour condamner les lieux. Attentat, travaux
d’urgence ? Non, expulsion des occupants de la Ruche de Croix-Rousse au n° 33
et des Pendarts les jardiniers pirates des n° 33bis et 35 de la rue. » Conféré
Le progrès 3 juin 2015.

Les arguments que l'on nous oppose ne tiennent pas :

## 1/ Sur la question de la sécurité:

Suite à l’occupation des Pendarts, la Ville de Lyon a interdit l’accès au
terrain par un arrêté municipal faisant référence à un autre arrêté
réglementant les zones exposées aux risques de mouvement de terrains sur Lyon.

« Un ingénieur géologue travaillant au sein d’une société lyonnaise de soutènements et fondations, a été confronté de nombreuses fois à la problématique dans l’exercice de sa profession. Sous couvert d’anonymat, il explique :
“Il y a une possibilité d’effondrement avéré, et la Ville de Lyon a pris ce risque au sérieux. La commission des balmes est un collège d’experts très sérieux, qui se prononce pour parer l’éventualité du risque sur les nouvelles constructions. Un effondrement peut arriver n’importe où, dans n’importe quel jardin alors qu’un habitant passe sa tondeuse.”
Pour cet ingénieur, il n’y a donc pas plus de risque d’effondrement au jardin des Pendarts qu’ailleurs sur les Pentes de la Croix-Rousse. »
(source : [http://www.rue89lyon.fr/2015/05/01/la-ville-de-lyon-part-labordage-du-jardin-pirate-des-pentes-de-la-croix-rousse](http://www.rue89lyon.fr/2015/05/01/la-ville-de-lyon-part-labordage-du-jardin-pirate-des-pentes-de-la-croix-rousse/))

## 2/ sur la question du squat

Nous ne nous considérons pas comme squatteurs mais comme des citoyens qui se
réapproprient des lieux appartenant à l'état inoccupés depuis plusieurs
années. Pour rappel, la Croix Rousse est située en plein centre ville et les
espaces verts peux nombreux. Quant à la maison, elle est tout à fait habitable
et pourrait donc accueillir des initiatives de quartier (atelier chorale,
yoga, dessin...) pour des personnes à très faibles revenus. D'autre part, en
quoi le fait de régulariser un squat a posteriori, ne serait-il pas tolérable?
« Le squat participe à la construction d'un modèle d'économie alternative. À
partir du XVIIe siècle, le terme squat apparaît pour désigner les occupations
illicites de terres par des paysans anglais, les Diggers. De l'abbé Pierre à
l'Organisation communiste libertaire (OCL) en passant par le comité des mal-
logés puis Droit au logement (DAL), nombreux sont ceux qui définissent le fait
de squatter comme l'expression d'un mouvement social revendiquant le droit à
une vie digne. »
[https://fr.wikipedia.org/wiki/Squat_(lieu)](https://fr.wikipedia.org/wiki/Squat_(lieu))

Durant les 4 mois d'ouverture au public, le jardin a été un lieu de rencontre,
d'échange et de partage entre les gens du quartier. Mais aussi un lieu
d'activité pour des personnes en situation d'isolement, un lieu pour récupérer
quelques légumes pour des personnes n'ayant plus de moyens financiers. D'autre
part, nous avons réhabilité et nettoyé un terrain abandonné, qui était souvent
squatté par une population peu intéressée par les questions d'écologie et de
citoyenneté.

C'est pourquoi, forts de nos soutiens sur le quartier, nous sollicitons vos
services pour poursuivre cette initiative qui ne coûte pas un sou à la
municipalité de Lyon et apporte beaucoup en bonheur de vivre dans le 1er
arrondissement de Lyon.

Un soutien et une action de votre part pourrait débloquer cette situation et
nous permettre de continuer cette magnifique initiative citoyenne.

Nous restons à votre disposition pour tout complément d'information et vous
prions d'agréer l'expression de nos sentiments les plus sincères.

Les Pendarts
Jardiniers Pirates Artivistes Extravagants
contact mail: lespendarts@free.fr